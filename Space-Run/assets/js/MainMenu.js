var beuGame = beuGame || {};
var numberOfStars = 150;

beuGame.MainMenu = function(){};

beuGame.MainMenu.prototype = {
  create: function() {
	  //set background

            stars = this.add.group();
            stars.enableBody = true;

            for(var i = 0; i < numberOfStars;i++){
               var s = stars.create(Math.floor((Math.random() * beuGame.game.width - 1) + 1), Math.floor((Math.random() * beuGame.game.height - 1) + 1), 'star');
                s.body.collideWorldBounds = true;
                s.body.onWorldBounds = new Phaser.Signal();
                s.body.onWorldBounds.add(endlessStars, this);
                s.body.velocity.setTo(-300, 0);
            }



	  //1 Spieler Game button
	  var buttonStartSelection0 = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.3, 'button', startSelection0, this, 2, 1, 0);
	  buttonStartSelection0.anchor.setTo(0.5,0.5);
	  buttonStartSelection0.width = buttonWith;
	  buttonStartSelection0.height = buttonHight;

	  var text0 = this.game.add.text(0, 0, "Spielen", buttonStyle);
	  text0.anchor.setTo(0.5,0.5);
	  text0.y = buttonStartSelection0.y;
	  text0.x = buttonStartSelection0.x;

	  //Optionen button
	  var buttonOptions = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.5, 'button', openOptions, this, 2, 1, 0);
	  buttonOptions.anchor.setTo(0.5,0.5);
	  buttonOptions.width = buttonWith;
	  buttonOptions.height = buttonHight;

	  var text2 = this.game.add.text(0, 0, "Optionen", buttonStyle);
	  text2.anchor.setTo(0.5,0.5);
	  text2.y = buttonOptions.y;
	  text2.x = buttonOptions.x;

	  //Credits button
	  var buttonCredits = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.7, 'button', openCredits, this, 2, 1, 0);
	  buttonCredits.anchor.setTo(0.5,0.5);
	  buttonCredits.width = buttonWith;
	  buttonCredits.height = buttonHight;

	  var text3 = this.game.add.text(0, 0, "Credits", buttonStyle);
	  text3.anchor.setTo(0.5,0.5);
	  text3.y = buttonCredits.y;
	  text3.x = buttonCredits.x;
  }
};

//Functions for the buttons

function startSelection0(){
	beuGame.game.state.start('Game');
}

function openOptions(){
	beuGame.game.state.start('Options');
}

function openCredits(){
	beuGame.game.state.start('Credits');
}
function endlessStars(sprite) {
    sprite.x = 799;
    sprite.y = Math.floor((Math.random() * beuGame.game.height - 1) + 1)
    sprite.body.velocity.setTo(-300, 0);
}
