var beuGame = beuGame || {};

beuGame.Preload = function(){};

beuGame.Preload.prototype = {
  preload: function() {
	  //
	  var text = this.game.add.text(beuGame.game.world.centerX,beuGame.game.world.centerY,"Loading...",{ font: 	"100px Arial" , fill:"#ffffff" ,align: "center"});
	  text.anchor.setTo(0.5,0.5);

 //Load images
	  this.load.image('button','assets/images/Button.png');
	  this.load.image('HB', 'assets/images/HB.png');
	  this.load.image('HBBackground', 'assets/images/HBBackground.png');
  	  this.load.image('HPBackgroundWhite', 'assets/images/HPBackgroundWhite.png');
  	  this.load.image('CreditsBackground', 'assets/images/CreditsBackground.png');
	  this.load.image('Startscreen', 'assets/images/Startscreen.png');

      this.load.image('bullet', 'assets/images/bullet.png');
      this.load.image('ship', 'assets/images/ship.png');
      this.load.image('star', 'assets/images/star.png');
      this.load.image('astroid', 'assets/images/astroit.png');
      this.load.image('explosion', 'assets/images/explosion.png');


      //Load audio
      this.load.audio("Backgroundmusic", 'assets/sounds/Backgroundmusic.wav');
      this.load.audio("Shot", 'assets/sounds/Shot.wav');
      this.load.audio("Hit", 'assets/sounds/Hit.wav');



  },
  create: function() {
  	this.state.start('MainMenu');
  },

	update: function() {
  	this.state.start('MainMenu');
  }
};
