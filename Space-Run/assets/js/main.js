var beuGame = beuGame || {};

var gameYsize = 1280;
var gameXsize = 720;

//size of the buttons
var buttonWith = 175;
var buttonHight = 50;

var musicVolume = 100;
var SFXVolume = 100;

var music;
var SFX;
var stars;

//The textstyle of the buttons
var buttonStyle = { font:"25px Arial" , fill:"#ffffff" ,align: "center"};

beuGame.game = new Phaser.Game(gameYsize,gameXsize, Phaser.AUTO, 'game-area');

beuGame.game.state.add('Boot', beuGame.Boot);
beuGame.game.state.add('Preload', beuGame.Preload);
beuGame.game.state.add('MainMenu', beuGame.MainMenu);
beuGame.game.state.add('Options', beuGame.Options);
beuGame.game.state.add('Credits', beuGame.Credits);
beuGame.game.state.add('Game', beuGame.Game);

beuGame.game.state.start('Boot');
