var beuGame = beuGame || {};

//Variabels

var maxHealth = 100;
var currHealth = 100;

var movespeed = 300;

var ship;
var weapon;
var cursors;
var fireButton;
var numberOfStars = 200;
var astroids;
var explosions;

var isPaused = false;
var isGameover = false;
var pauseKey;
var pauseText;
var buttomMainMenu;
var textMainMenu;
var div = 75;
var prescaler = 0;

var buttomRestart;
var textRestart;

var textScore;
var textTime;

var time = 0;

var score;

var score = 00;

var moveHorizontal = 0;
var moveVertikal = 0;

var wKey;
var aKey;
var sKey;
var dey;

beuGame.Game = function(){};

beuGame.Game.prototype = {

	create: function() {

        score = 0;
        time = 0;
        isGameover = false;
        isPaused = false;
        numberOfStars = 200;
        div = 75;
        prescaler = 0;
        this.physics.startSystem(Phaser.Physics.ARCADE);

        Backgroundmusic = this.game.add.audio('Backgroundmusic',musicVolume/100,true);
		Hit = this.game.add.audio('Hit',SFXVolume/100,false);
		Shot = this.game.add.audio('Shot',SFXVolume/100,false);

		Backgroundmusic.play();
        this.game.time.events.loop(1000, divCalculation, this);


        stars = this.add.group();
            stars.enableBody = true;

            for(var i = 0; i < numberOfStars;i++){
               var s = stars.create(Math.floor((Math.random() * beuGame.game.width - 1) + 1), Math.floor((Math.random() * beuGame.game.height - 1) + 1), 'star');
                s.body.collideWorldBounds = true;
                s.body.onWorldBounds = new Phaser.Signal();
                s.body.onWorldBounds.add(endlessStars, this);
                s.body.velocity.setTo(-300, 0);
            }

        astroids = this.add.group();
            astroids.enableBody = true;


        //Keys
        cursors = this.input.keyboard.createCursorKeys();

        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        wKey = this.input.keyboard.addKey(Phaser.Keyboard.W)
        aKey = this.input.keyboard.addKey(Phaser.Keyboard.A)
        sKey = this.input.keyboard.addKey(Phaser.Keyboard.S)
        dKey = this.input.keyboard.addKey(Phaser.Keyboard.D)



        pauseText = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.3
		,"Pausiert",{ font:"60px Arial" , fill:"#ffffff" ,align: "center"});
		pauseText.anchor.setTo(0.5,0.5);
        pauseText.visible = false;

        textScore = this.game.add.text(beuGame.game.width*0.25, beuGame.game.height * 0.1, "Score: " + score, buttonStyle);
        textScore.anchor.setTo(0.5,0.5);

        textTime = this.game.add.text(beuGame.game.width*0.75, beuGame.game.height * 0.1, "Time: " + time, buttonStyle);
        textTime.anchor.setTo(0.5,0.5);


        buttomMainMenu = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.5, 'button', backToMainMenu, this, 2, 1, 0);
        buttomMainMenu.anchor.setTo(0.5,0.5);
        buttomMainMenu.width = buttonWith;
        buttomMainMenu.height = buttonHight;
        buttomMainMenu.visible = false;

        textMainMenu = this.game.add.text(0, 0, "Hauptmenü", buttonStyle);
        textMainMenu.anchor.setTo(0.5,0.5);
        textMainMenu.y = buttomMainMenu.y;
        textMainMenu.x = buttomMainMenu.x;
        textMainMenu.visible = false;

        buttomRestart = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.7, 'button', restart, this, 2, 1, 0);
        buttomRestart.anchor.setTo(0.5,0.5);
        buttomRestart.width = buttonWith;
        buttomRestart.height = buttonHight;
        buttomRestart.visible = false;

        textRestart = this.game.add.text(0, 0, "Restart", buttonStyle);
        textRestart.anchor.setTo(0.5,0.5);
        textRestart.y = buttomRestart.y;
        textRestart.x = buttomRestart.x;
        textRestart.visible = false;


        weapon = this.add.weapon(40, 'bullet');

        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletAngleOffset = 180;
        weapon.bulletSpeed = 400;
        weapon.fireRate = 120;
        weapon.bulletAngleVariance = 3;
        weapon.onFire.add(function() {Shot.play()},this);

        cursors = this.input.keyboard.createCursorKeys();

        sprite = this.add.sprite(beuGame.game.width/4, beuGame.game.height/2, 'ship');
        sprite.anchor.set(0.5);

        this.physics.arcade.enable(sprite);
        sprite.body.collideWorldBounds = true;
        weapon.trackSprite(sprite, 25, 0,true);
	},



	update: function() {
    if(isPaused){

    }else{
        setScoreTime();

        sprite.body.velocity.x = 0;
        sprite.body.velocity.y = 0;
        moveVertikal = 0;
        moveHorizontal = 0;

        if (cursors.left.isDown || aKey.isDown){
            moveHorizontal = moveHorizontal - 1;
        }
        if (cursors.right.isDown || dKey.isDown){
            moveHorizontal = moveHorizontal + 1;
        }
        if (cursors.up.isDown || wKey.isDown){
            moveVertikal = moveVertikal - 1;
        }
        if (cursors.down.isDown || sKey.isDown){
            moveVertikal = moveVertikal + 1;
        }

        if(moveHorizontal == 1){
            sprite.body.velocity.x  = 200;
        }
        if(moveHorizontal == -1){
            sprite.body.velocity.x = - 200;
        }
        if(moveVertikal == 1){
            sprite.body.velocity.y = 200;
        }
        if(moveVertikal == -1){
            sprite.body.velocity.y = - 200;
        }


    //    console.log(moveHorizontal);
    //    console.log(moveVertikal);

        if (fireButton.isDown)
        {
            weapon.fire();
        }

        if(sprite.body.x > (beuGame.game.width / 2)){
            sprite.body.x = (beuGame.game.width / 2);
        }

        this.physics.arcade.collide(sprite, astroids, gameOver, null, this);
        this.physics.arcade.collide(weapon.bullets, astroids, collisionHandler, null, this);
        }

        if(Math.floor((Math.random() * div) + 1) == 1)
        {
            var e = astroids.create(Math.floor((Math.random() * beuGame.game.width * 2) + beuGame.game.width + 50), Math.floor((Math.random() * beuGame.game.height + 30) + 30), 'astroid');
                e.body.velocity.setTo(-300 + div, 0);
        }
    }

}

function endlessStars(sprite) {
    sprite.x = beuGame.game.width - 1;
    sprite.y = Math.floor((Math.random() * beuGame.game.height - 1) + 1)
    sprite.body.velocity.setTo(-300, 0);
}

window.onkeydown = function(event) {
	if (event.keyCode == 80 && !isGameover){
		if(isPaused == false){
			isPaused = true
			beuGame.game.paused = true;
			pauseText.visible = true;
            buttomMainMenu.visible = true;
            textMainMenu.visible = true;
            buttomRestart.visible = true;
            textRestart.visible = true;
		}else{
			isPaused = false;
			beuGame.game.paused = false;
			pauseText.visible = false;
            buttomMainMenu.visible = false;
            textMainMenu.visible = false;
            buttomRestart.visible = false;
            textRestart.visible = false;
		}
	}
}

function backToMainMenu(){
	beuGame.game.state.start('MainMenu');
    isPaused = false;
	beuGame.game.paused = false;
	pauseText.visible = false;
    buttomMainMenu.visible = false;
    textMainMenu.visible = false;
    buttomRestart.visible = false;
    textRestart.visible = false;
}

function restart(){
	beuGame.game.state.start('Game');
    isPaused = false;
	beuGame.game.paused = false;
	pauseText.visible = false;
    buttomMainMenu.visible = false;
    textMainMenu.visible = false;
    buttomRestart.visible = false;
    textRestart.visible = false;
}

function gameOver(){
    isPaused = true
    beuGame.game.paused = true;
    pauseText.visible = true;
    pauseText.setText("GameOver");
    buttomMainMenu.visible = true;
    textMainMenu.visible = true;
    buttomRestart.visible = true;
    textRestart.visible = true;
    isGameover = true;
}

function collisionHandler (obj1, obj2) {

    obj1.kill();
    obj2.kill();
    var s = stars.create(obj2.x, obj2.y, 'explosion');
    s.lifespan = 50;
    score = score + 5;
    Hit.play();

}


function setScoreTime() {

    textScore.text = "Score: " + score;
    textTime.text = "Time: " + time;

}
function divCalculation(){
    time = time + 1;
    if(prescaler > 1){
            prescaler = 0
        if(div > 1){div = div - 1;}
            console.log(div);
        }
        else{
            prescaler = prescaler + 1;
        }
}
