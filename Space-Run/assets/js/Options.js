var beuGame = beuGame || {};

var SFXVolText;
var MusicVolText;
var numberOfStars = 150;

beuGame.Options = function(){};

beuGame.Options.prototype = {
  create: function() {
	  //Set background
stars = this.add.group();
        stars.enableBody = true;

        for(var i = 0; i < numberOfStars;i++){
           var s = stars.create(Math.floor((Math.random() * beuGame.game.width - 1) + 1), Math.floor((Math.random() * beuGame.game.height - 1) + 1), 'star');
            s.body.collideWorldBounds = true;
            s.body.onWorldBounds = new Phaser.Signal();
            s.body.onWorldBounds.add(endlessStars, this);
            s.body.velocity.setTo(-300, 0);
        }
	  //Main Menu button
	  var backButon = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.75, 'button', backToMainMenu, this, 2, 1, 0);
	  backButon.anchor.setTo(0.5,0.5);
	  backButon.width = buttonWith;
	  backButon.height = buttonHight;

	  var text0 = this.game.add.text(0, 0, "Hauptmenü", buttonStyle);
	  text0.anchor.setTo(0.5,0.5);
	  text0.y = backButon.y;
	  text0.x = backButon.x;

	  //shows the SFX Volume
	  SFXVolText = beuGame.game.add.text(beuGame.game.world.centerX,beuGame.game.height*0.25,"SFX: " + SFXVolume,{ font: "25px Arial" ,fill:"#ffffff" ,align: "center"});
	  SFXVolText.anchor.setTo(0.5,0.5);

	  //SFX Volume up
	  var SFXVolUp = this.game.add.button(beuGame.game.world.centerX + 150, beuGame.game.height*0.25, 'button', SFXUp, this, 2, 1, 0);
	  SFXVolUp.anchor.setTo(0.5,0.5);
	  SFXVolUp.width = buttonHight;
	  SFXVolUp.height = buttonHight;

	  var text0 = beuGame.game.add.text(0, 0, ">", buttonStyle);
	  text0.anchor.setTo(0.5,0.5);
	  text0.y = SFXVolUp.y;
	  text0.x = SFXVolUp.x;

	  //SFX Volume down
	  var SFXVolDown = this.game.add.button(beuGame.game.world.centerX - 150, beuGame.game.height*0.25, 'button', SFXDown, this, 2, 1, 0);
	  SFXVolDown.anchor.setTo(0.5,0.5);
	  SFXVolDown.width = buttonHight;
	  SFXVolDown.height = buttonHight;

	  var text1 = beuGame.game.add.text(0, 0, "<", buttonStyle);
	  text1.anchor.setTo(0.5,0.5);
	  text1.y = SFXVolDown.y;
	  text1.x = SFXVolDown.x;

	  //shows the Music Volume
	  MusicVolText = beuGame.game.add.text(beuGame.game.world.centerX,beuGame.game.height*0.5,"Music: " + musicVolume,{ font: "25px Arial" ,fill:"#ffffff" ,align: "center"});
	  MusicVolText.anchor.setTo(0.5,0.5);

	  //Music Volume up
	  var MusicVolUp = this.game.add.button(beuGame.game.world.centerX + 150, beuGame.game.height*0.5, 'button', MusicUp, this, 2, 1, 0);
	  MusicVolUp.anchor.setTo(0.5,0.5);
	  MusicVolUp.width = buttonHight;
	  MusicVolUp.height = buttonHight;

	  var text2 = beuGame.game.add.text(0, 0, ">", buttonStyle);
	  text2.anchor.setTo(0.5,0.5);
	  text2.y = MusicVolUp.y;
	  text2.x = MusicVolUp.x;

	  //Music Volume down
	  var MusicVolDown = this.game.add.button(beuGame.game.world.centerX - 150, beuGame.game.height*0.5, 'button', MusicDown, this, 2, 1, 0);
	  MusicVolDown.anchor.setTo(0.5,0.5);
	  MusicVolDown.width = buttonHight;
	  MusicVolDown.height = buttonHight;

	  var text3 = beuGame.game.add.text(0, 0, "<", buttonStyle);
	  text3.anchor.setTo(0.5,0.5);
	  text3.y = MusicVolDown.y;
	  text3.x = MusicVolDown.x;

    },

};

//Functions for the buttons

function backToMainMenu(){
	beuGame.game.state.start('MainMenu');
}

function SFXUp(){
	if(SFXVolume<100){
		SFXVolume++;
		SFXVolText.setText("SFX: " + SFXVolume);
		//console.log("SFX: " + SFXVolume);
		localStorage.setItem('SFXVolume', JSON.stringify(SFXVolume));
	}
}

function SFXDown(){
		if(SFXVolume>0){
		SFXVolume--;
		SFXVolText.setText("SFX: " + SFXVolume);
		//console.log("SFX: " + SFXVolume);
			localStorage.setItem('SFXVolume', JSON.stringify(SFXVolume));
	}
}

function MusicUp(){
	if(musicVolume<100){
		musicVolume++;
		MusicVolText.setText("Music: " + musicVolume);
		//console.log("Music: " + musicVolume);
		localStorage.setItem('musicVolume', JSON.stringify(musicVolume));
	}
}

function MusicDown(){
		if(musicVolume>0){
		musicVolume--;
		MusicVolText.setText("Music: " + musicVolume);
		//console.log("Music: " + musicVolume);
		localStorage.setItem('musicVolume', JSON.stringify(musicVolume));
	}
}
