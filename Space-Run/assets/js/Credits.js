var beuGame = beuGame || {};
var Style = { font:"20px Arial" , fill:"#ffffff" ,align: "center"};
var StyleTitle1 = { font:"30px Arial" , fill:"#ffffff" ,align: "center"};
var StyleTitle2 = { font:"25px Arial" , fill:"#ffffff" ,align: "center"};
var numberOfStars = 150;


beuGame.Credits = function(){};

beuGame.Credits.prototype = {

  create: function() {

	  //Set Background

	  //Main Menu button
      stars = this.add.group();
        stars.enableBody = true;

        for(var i = 0; i < numberOfStars;i++){
           var s = stars.create(Math.floor((Math.random() * beuGame.game.width - 1) + 1), Math.floor((Math.random() * beuGame.game.height - 1) + 1), 'star');
            s.body.collideWorldBounds = true;
            s.body.onWorldBounds = new Phaser.Signal();
            s.body.onWorldBounds.add(endlessStars, this);
            s.body.velocity.setTo(-300, 0);
        }

	  var buttonMainMenu1 = this.game.add.button(beuGame.game.world.centerX, beuGame.game.height * 0.9, 'button', backToMainMenu, this, 2, 1, 0);
	  buttonMainMenu1.anchor.setTo(0.5,0.5);
	  buttonMainMenu1.width = buttonWith;
	  buttonMainMenu1.height = buttonHight;

	  var text0 = beuGame.game.add.text(0, 0, "Hauptmenü", buttonStyle);
	  text0.anchor.setTo(0.5,0.5);
	  text0.y = buttonMainMenu1.y;
	  text0.x = buttonMainMenu1.x;


	  var background2 = this.game.add.sprite(beuGame.game.world.centerX, beuGame.game.height * 0.05, 'button');
	  background2.anchor.setTo(0.5,0);
	  background2.width = 300;
	  background2.height = beuGame.game.height * 0.6;


	  var text1 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.1, "Credits", StyleTitle1);
	  text1.anchor.setTo(0.5,0.5);

	  var text2 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.2, "Programming", StyleTitle2);
	  text2.anchor.setTo(0.5,0.5);

	  var text3 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.3, "Lukas Fritsch", Style);
	  text3.anchor.setTo(0.5,0.5);

	  var text4 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.4, "Elias Blum", Style);
	  text4.anchor.setTo(0.5,0.5);

	  var text5 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.5, "Design", StyleTitle2);
	  text5.anchor.setTo(0.5,0.5);

	  var text6 = this.game.add.text(beuGame.game.world.centerX, beuGame.game.height * 0.6, "Michael Brida", Style);
	  text6.anchor.setTo(0.5,0.5);

  	}
};


//Logo Links
function PhaserLink(){
	 window.location.href = "http://www.phaser.io";
}

function ViennaLink(){
	 window.location.href = "http://www.vienna.at";
}

function MusicLink(){
	 window.location.href = "http://www.littlerobotsoundfactory.com/";
}
function endlessStars(sprite) {
    sprite.x = 799;
    sprite.y = Math.floor((Math.random() * beuGame.game.height - 1) + 1)
    sprite.body.velocity.setTo(-300, 0);
}
