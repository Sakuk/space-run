var beuGame = beuGame || {};

beuGame.Boot = function(){};

beuGame.Boot.prototype = {
  preload: function() {

	  //Save Volume settings
	  var firstVisit = false;
	  if(localStorage.getItem('firstVisit') != "false"){
	  localStorage.setItem('musicVolume', JSON.stringify(musicVolume));
	  localStorage.setItem('SFXVolume', JSON.stringify(SFXVolume));
	  //console.log("first visite");
		  localStorage.setItem('firstVisit', firstVisit);
	  }else{
		  musicVolume = JSON.parse(localStorage.getItem('musicVolume'));
		  SFXVolume = JSON.parse(localStorage.getItem('SFXVolume'));
		  //console.log("first visite = false");
	  }
  },
  create: function() {

    this.game.stage.backgroundColor = '#000000';

    //start preload
    this.state.start('Preload');
  }
};
